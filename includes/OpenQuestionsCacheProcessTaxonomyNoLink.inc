<?php

/**
 * @file
 * Contains a helper class for OpenQuestions.
 */

/**
 * Extends OpenQuestionsCacheProcess to process as unlinked taxonomy term names.
 */
class OpenQuestionsCacheProcessTaxonomyNoLink extends OpenQuestionsCacheProcess {
  /**
   * Process as check_plain'ed term names.
   */
  public function process() {
    $temp_tids = $this->getKeys();
    if (!$temp_tids) {
      return;
    }

    $all_terms = taxonomy_term_load_multiple($temp_tids);

    foreach ($all_terms as $tid => $term) {
      $this->processed[$tid] = check_plain($term->name);
    }
  }

}
