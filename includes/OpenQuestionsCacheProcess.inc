<?php

/**
 * @file
 * Contains a helper class for OpenQuestions.
 */

/**
 * A simple cache where items can be processed after they're added.
 */
class OpenQuestionsCacheProcess {
  protected $data;
  protected $processed;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->data = array();
    $this->processed = array();
  }

  /**
   * Add an item.
   */
  public function add($k, $v) {
    $this->data[$k] = $v;
  }

  /**
   * Returns whether an item with that key exists.
   */
  public function has($k) {
    return !empty($this->processed[$k]) || !empty($this->data[$k]);
  }

  /**
   * Returns an item or NULL if it doesn't exist.
   */
  public function get($k, $default = NULL) {
    return !empty($this->processed[$k]) ? $this->processed[$k] : (!empty($this->data[$k]) ? $this->data[$k] : $default);
  }

  /**
   * Returns all items.
   */
  public function getAll() {
    return !empty($this->processed) ? $this->processed : $this->data;
  }

  /**
   * Returns all keys.
   */
  public function getKeys() {
    return array_keys($this->data);
  }

  /**
   * Process each item.
   */
  public function process() {
  }

}
