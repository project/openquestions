<?php

/**
 * @file
 * Contains a helper class for OpenQuestions.
 */

/**
 * Used to construct the table on user profile pages.
 */
class OpenQuestionsUserTable {
  private $account = NULL;
  private $expertiseTids = NULL;
  private $showMoreLink = NULL;
  private $moreString = NULL;
  private $agoString = NULL;
  private $curTime = NULL;
  private $rowQueryCache = NULL;
  private $actionCache = NULL;
  private $authorCache = NULL;
  private $topicCache = NULL;
  private $reporttypeCache = NULL;

  /**
   * Constructor.
   */
  public function __construct($acct, $tids, $show_more) {
    $this->account = $acct;
    $this->expertiseTids = $tids;
    $this->showMoreLink = $show_more;
    $this->curTime = time();
    $this->moreString = t('More');
    $this->agoString = ' ' . t('ago');

    $this->rowQueryCache = new OpenQuestionsCacheProcess();
    $this->actionCache = new OpenQuestionsCacheProcess();
    $this->authorCache = new OpenQuestionsCacheProcessUser();
    $this->topicCache = new OpenQuestionsCacheProcessTaxonomy();
    $this->reporttypeCache = new OpenQuestionsCacheProcessTaxonomyNoLink();
  }

  /**
   * Returns the rows of the table.
   */
  public function getRows() {
    $query = db_select('node', 'n');
    $query->join('field_data_field_oq_topic', 'topic', 'topic.entity_id = n.nid');
    $query->join('taxonomy_term_data', 'term', 'term.tid = topic.field_oq_topic_tid');
    $query->join('users', 'user', 'user.uid = n.uid');
    $query->fields('n', array('nid', 'title', 'uid', 'changed'));
    $query->fields('term', array('tid', 'name'));
    $query->fields('user', array('uid'));
    $query->addField('user', 'name', 'author_name');
    $query->addTag('node_access');
    $query->condition('n.status', 1);
    $query->condition('n.type', OpenQuestionsCommon::getQuestionNodeType());
    $query->condition('topic.field_oq_topic_tid', $this->expertiseTids, 'IN');

    $table_sort = $query->extend('TableSort')->orderByHeader($this->getHeader());
    $table_sort->extend('PagerDefault')->limit(20);

    $result = $query->execute();

    foreach ($result as $row) {
      $this->topicCache->add($row->tid, $row->name);
      $this->authorCache->add($row->uid, $row->author_name);
      $this->rowQueryCache->add($row->nid, $row);
    }

    $this->topicCache->process();
    $this->authorCache->process();

    $query = db_select('while_base', 'wb');
    $query->join('field_data_field_oq_forquestion', 'forquestion', 'forquestion.entity_id = wb.id');
    $query->join('field_data_field_oq_fromuser', 'fromuser', 'fromuser.entity_id = wb.id');
    $query->leftJoin('field_data_field_oq_vote', 'vote', 'vote.entity_id = forquestion.entity_id');
    $query->leftJoin('field_data_field_oq_reporttype', 'reporttype', 'reporttype.entity_id = forquestion.entity_id');
    $query->fields('wb', array('id'));
    $query->addField('wb', 'changed', 'entity_changed');
    $query->fields('forquestion', array('field_oq_forquestion_target_id'));
    $query->fields('vote', array('field_oq_vote_value'));
    $query->fields('reporttype', array('field_oq_reporttype_tid'));
    $query->condition('forquestion.field_oq_forquestion_target_id', $this->rowQueryCache->getKeys(), 'IN');
    $query->condition('fromuser.field_oq_fromuser_uid', $this->account->uid);

    $result = $query->execute();

    foreach ($result as $row) {
      $this->actionCache->add($row->field_oq_forquestion_target_id, $row);
      $this->reporttypeCache->add($row->field_oq_reporttype_tid, 1);
    }

    $this->reporttypeCache->process();

    $rows = array();

    $all_rows = $this->rowQueryCache->getAll();
    foreach ($all_rows as $row) {
      $rows[] = $this->makeRow($row);
    }

    return $rows;
  }

  /**
   * Make an individual row.
   */
  public function makeRow($row) {
    $linked_title = l($row->title, 'node/' . $row->nid);
    $ago_node = format_interval($this->curTime - $row->changed, 1) . $this->agoString;
    $action = $this->actionCache->get($row->nid);

    if ($this->showMoreLink && !empty($action->id)) {
      $linked_entity = l($this->moreString, 'oq_uservote/' . $action->id);
    }
    else {
      $linked_entity = '--';
    }

    if ($action && !empty($action->entity_changed)) {
      $ago_entity = format_interval($this->curTime - $action->entity_changed, 1) . $this->agoString;
    }
    else {
      $ago_entity = '--';
    }

    if ($action && !empty($action->field_oq_reporttype_tid) && $this->reporttypeCache->has($action->field_oq_reporttype_tid)) {
      $reporttype = $this->reporttypeCache->get($action->field_oq_reporttype_tid);
    }
    else {
      $reporttype = '--';
    }

    if ($action && !empty($action->field_oq_vote_value)) {
      $vote = $action->field_oq_vote_value;
    }
    else {
      $vote = '--';
    }

    if (!empty($row->tid) && $this->topicCache->has($row->tid)) {
      $topic = $this->topicCache->get($row->tid);
    }
    else {
      $topic = '--';
    }

    if (!empty($row->uid) && $this->authorCache->has($row->uid)) {
      $author = $this->authorCache->get($row->uid);
    }
    else {
      $author = '--';
    }

    return array(
      $topic,
      $linked_title,
      $author,
      $ago_node,
      $vote,
      $reporttype,
      $ago_entity,
      $linked_entity,
    );
  }

  /**
   * Returns the header of the table.
   */
  public function getHeader() {
    $header = array(
      array('data' => t('Topic'), 'field' => 'field_oq_topic_tid'),
      array('data' => t('Title'), 'field' => 'title'),
      array('data' => t('Author'), 'field' => 'uid'),
      array('data' => t('Post date'), 'field' => 'changed'),
      array('data' => t('Vote')),
      array('data' => t('Report')),
      array('data' => t('Action date')),
      array('data' => t('More')),
    );

    return $header;
  }

}
