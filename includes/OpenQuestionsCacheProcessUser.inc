<?php

/**
 * @file
 * Contains a helper class for OpenQuestions.
 */

/**
 * Extends OpenQuestionsCacheProcess to process as links to user profiles.
 */
class OpenQuestionsCacheProcessUser extends OpenQuestionsCacheProcess {
  /**
   * Process as links to user profile pages.
   */
  public function process() {
    foreach ($this->data as $uid => $name) {
      $this->processed[$uid] = l($name, 'user/' . $uid);
    }
  }

}
