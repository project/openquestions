<?php

/**
 * @file
 * Contains a helper class for OpenQuestions.
 */

/**
 * Extends OpenQuestionsCacheProcess to process as taxonomy term links.
 */
class OpenQuestionsCacheProcessTaxonomy extends OpenQuestionsCacheProcess {
  /**
   * Process as links to taxonomy term pages.
   */
  public function process() {
    foreach ($this->data as $tid => $name) {
      $this->processed[$tid] = l($name, 'taxonomy/term/' . $tid);
    }
  }

}
