<?php

/**
 * @file
 * Contains admin functions for OpenQuestions.
 */

/**
 * Settings form for this module.
 *
 * @see system_settings_form()
 */
function openquestions_settings() {
  $form = array();

  $form['openquestions_add_social_media_names'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show social media names for the socialmedia module?'),
    '#default_value' => variable_get('openquestions_add_social_media_names', FALSE),
    '#description' => t("The socialmedia module is used to manage a user's social media links (Linkedin, etc.) that are shown on the user's profiles. That module needs an external library to be installed in order to show icons in its form. If you install that external library (see https://www.drupal.org/project/socialfield) uncheck this since the icons will appear. However if you don\'t want to install the external library, then check this and the names of the services will appear. NOTE: after changing this setting, clear the cache at admin/config/development/performance for the changes to appear."),
  );

  return system_settings_form($form);
}
