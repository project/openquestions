CONTENTS OF THIS FILE
---------------------

* Brief description  
* Quick installation
* Full description
* Requirements
* Recommended modules
* Installation notes
* Configuration
* Possible conflicts
* Maintainers


BRIEF DESCRIPTION
------------

OpenQuestions is a Features-based project designed to help hold politicians
accountable by encouraging people to ask them very tough questions about
their policies at their public appearances.

It installs a "Question" content type, vocabularies, an entity that
represents an individual vote, an "Expert" role, and views. Visitors submit
Question nodes and then those in the Expert role vote on those questions (or
report them if necessary). All votes and reports cast by Experts are public,
revisioned, and can be commented on. That helps hold Experts accountable and
ensures that only the toughest questions rise to the top.

By changing the vocabulary terms, this module can be used for other "best
instead of most popular" sites, such as picking the best rather than most
popular singer, etc.


QUICK INSTALLATION
------------

* For the easiest full installation, download all of the modules in the
   Requirements section below, and then simply enable all three of the modules
   in the OpenQuestions section on admin/modules.

* After doing that, it's recommended to clear the cache. Visit this page
   on your site and press the "Clear all caches" button:
   admin/config/development/performance

* Then, see the Configuration section below. It describes how to test out the
   project.

* If you need more information on installing Drupal modules, see this page:
   https://drupal.org/documentation/install/modules-themes/modules-7


FULL DESCRIPTION
------------

Politicians frequently promote unrealistic policies and the media doesn't
call them on the downsides of those policies. The OpenQuestions project
is one part of the way to finally hold those politicians accountable.

OpenQuestions installs a voting system designed to sort the toughest of
user-submitted questions. Unlike similar past efforts like the White House's
Open For Questions, the questions are voted up by how hard they are to
answer, not how popular they are.

After the toughest questions have been selected, you the site owner
encourage others to go to public appearances and ask those questions.
Getting the questions answered is up to you; this project is only designed
to help choose the best (not necessarily the most popular) questions.

OpenQuestions is Features-based and installs a Question content type.
Visitors to your site submit Question nodes, and then those in the Expert
role vote on how tough those questions are. Unlike other systems, each
vote is public. That encourages the Experts to vote according to whether
a question is tough, not just whether they agree with it.

Experts can also mark a question as spam, a duplicate, etc.

Not only are all actions by Experts public, but so too are which questions
in their field that they've elected not to answer.

With the provided OpenQuestions Disqus module, Disqus comments appear on
Question nodes, on user profiles, and on individual votes.

All of that serves to hold Experts accountable and to ensure that they
aren't voting down (or ignoring) tough questions because they disagree
with them while voting up weak questions because they agree with them.

Note that this system is ideology-neutral: it can be used to advocate for
or against any topic or topics of interest to you. It can also be used
for non-political topics if you wish.

* For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/tolstoydotcom/2467883

* To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2467883


REQUIREMENTS
------------
This module requires the following modules (directly or indirectly):

* Chaos Tools (https://drupal.org/project/ctools)
* Entity API (https://drupal.org/project/entity)
* Entity Reference (https://drupal.org/project/entity_reference)
* Features (https://drupal.org/project/features)
* Features Roles Permissions
   (https://drupal.org/project/features_roles_permissions)
* Field Group (https://drupal.org/project/field_group)
* Pathauto (https://drupal.org/project/pathauto)
* Pathauto Entity (https://drupal.org/project/pathauto_entity)
* References (https://drupal.org/project/references)
* Social field (https://drupal.org/project/socialfield)
* Strongarm (https://drupal.org/project/strongarm)
* Token (https://drupal.org/project/token)
* Universally Unique ID (https://drupal.org/project/uuid)
* UUID Features (https://drupal.org/project/uuid_features)
* Views (https://drupal.org/project/views)
* While (https://drupal.org/project/while)


RECOMMENDED MODULES
-------------------
As installed, the social media text fields on the user register page
won't have icons for the various services (Twitter, etc.) However, this
module changes that form to show the text names of the services. If you'd
prefer the icons instead, see the note about Fontello on the
https://drupal.org/project/socialfield page.


INSTALLATION NOTES
------------

* If you did the quick installation above, you can skip this section.

* This project consists of four modules:
   The OpenQuestions features module is in the Features section on
   admin/modules. This module defines the taxonomy terms, roles, etc.
   used by the project.
   The other three modules are in the OpenQuestions section on
   admin/modules: the main module, the OpenQuestions Disqus
   module, and OpenQuestions Common module.

* For the easiest full installation, simply enable all three of the modules
   in the OpenQuestions section on admin/modules. That will also enable the
   OpenQuestions features module and prompt you if there are any missing
   contributed modules that are required.

* If you don't want comments on the nodes/users/entities used by this
   project, then you don't need to enable the OpenQuestions Disqus module.
   However, that isn't recommended as comments will help hold Experts
   accountable. You can also provide your own commenting system (see the
   Reply, Status, and related projects). Those have not been tested with
   the OpenQuestions project.

* If desired for your own purposes, you can also edit the openquestions.info
   file and remove the dependency on the openquestions_features module, and
   then choose not to enable the latter module. However, you'll need to
   provide your own alternatives to the content types, the entity type, the
   views, and so on that are provided by the openquestions_features module.


CONFIGURATION
------------

* Visit admin/structure/block and place the "View: Node votes" and "View: Node
   reports" blocks where you want them. Those show the votes and reports on
   Question node pages. The main OpenQuestions module keeps those blocks from
   appearing on other pages, so you don't have to configure the visibility of
   those blocks yourself.

* To test out the system, login as an admin user and add the oq_expert role to
   your account. On your user edit page, scroll down to the "Expertise"
   section and check "Miscellaneous".
   (See https://www.drupal.org/node/120614 if you aren't familiar with how
   to do those).

   Then, visit node/add/oq-question and enter a test question. In the "Topic"
   select, choose "Miscellaneous". On the "Publishing Options" tab, check
   "Published". Then, press "Save".

   Because you placed yourself in the oq_expert role, you'll now see a voting
   form on the page for your new node. You'll also see two blocks listing
   votes and reports for the current node. If you vote, the vote will appear
   in the "Node votes" block, and if you report the node it will appear in
   the "Node reports" block.

   In the "Node votes" block, the time (like "1 minute ago") of the vote
   will be linked to a page just for your votes on that specific question.

   Also, on your user page a table will show all published questions in your
   field of expertise, even questions you haven't voted on or reported.
   Note: that explains the choices of "Miscellaneous" above: the question
   you submitted is in your field of expertise and is thus shown on your user
   page.

   That serves as another way visitors can hold Experts accountable: visitors
   can see which questions in their field of expertise Experts have voted on
   or haven't voted on yet.

* Visit admin/config/services/openquestions to adjust settings for the project.
   As discussed in the RECOMMENDED MODULES section above, you can choose to
   have social network icons on the user/register page, or choose to just use
   the names of those networks.

* If you enabled the OpenQuestions: Disqus module, visit
   admin/config/services/openquestions/disqus to adjust the settings for that
   module. You'll need to sign up for that service and enter the handle they
   provide.

* Visit admin/people/permissions to adjust the permissions. The permissions for
   the oq_expert role are already set, but you may want to adjust them.

   If you want to allow people to submit questions (recommended) then give
   the authenticated user role the "Question: Create new content" permission.
   You can also choose to give authenticated users the "Question: Edit own
   content" permission, but that might result in a question being changed after
   Experts have voted on it.

   You will probably want to give all roles the "View action pages" and
   "View action history" permissions.

* The Question and Supporting Information text areas for Question nodes allow
   the author to select the text format they want to use. You'll need to make
   sure that the roles that are allowed to post questions can't use unsafe
   text formats (like Full HTML). Visit admin/config/content/formats to see
   which roles can use which formats.

* You might also want to create a block for the front page or a front page node
   inviting people to submit questions, and linking to the node/add/oq-question
   form.

* Note that as installed, Question nodes are set to be unpublished. It's up
   to you to review the list on admin/content for unpublished questions that
   have been submitted by visitors and then publishing them. (In a future
   version, Rules may be provided to optionally send emails to administrators
   about new submissions.)

* The user/register form includes an "Apply for expert status" checkbox. You'll
   want to monitor new users to see who wants to apply and then decide whether
   or not to put them into the oq_expert role.
   (In a future version, Rules may be provided to optionally send emails to
   administrators about such users.)

* On the admin/structure/taxonomy page, you can add or delete items from the
   Report type and Topic vocabularies (as long as you don't delete the
   vocabularies themselves).

* You'll have to monitor questions that Experts have reported by visiting the
   latest-reports page. What actions you take based on those reports are left
   to you.

* If installing this project into a new site, you'll probably want to enable
   the Views UI module on admin/modules.


POSSIBLE CONFLICTS
-----------

Most of the views, fields, etc. used in this project have machine names
prefixed with "oq_". So, even if you already have a node type of "Question"
installed, it probably doesn't have a machine name of "oq_question" and thus
there shouldn't be a conflict installing this project into an existing site.

However, there may be conflicts with this strongarm setting:
pathauto_user_pattern

If you already have the pathauto_entity module installed, there may be
conflicts with these strongarm settings:
pathauto_entity_available_entity_types
pathauto_entity_bundles

If there might be such conflicts, you can either edit the file
openquestions_features/openquestions_features.strongarm.inc with a text
editor to change those settings, or edit the pathauto settings starting on
the page admin/config/search/path.
 

MAINTAINERS
-----------

Current maintainers:
* Chris Kelly - https://www.drupal.org/u/tolstoydotcom
