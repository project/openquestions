/**
 * @file
 * Adapted from the Disqus Drupal module.
 */

// The Disqus global variables.
var disqus_shortname = '';
var disqus_url = '';
var disqus_title = '';
var disqus_identifier = '';

(function ($) {
    /**
     * Drupal Disqus behavior.
     */
    Drupal.behaviors.openquestions_disqus = {
        attach: function (context, settings) {
            $('body').once('disqus_thread', function() {
                if (settings.openquestions_disqus || false) {
                    // Setup the global JavaScript variables for Disqus.
                    disqus_shortname = settings.openquestions_disqus.shortname;
                    disqus_url = settings.openquestions_disqus.url;
                    disqus_title = settings.openquestions_disqus.title;
                    disqus_identifier = settings.openquestions_disqus.identifier;
                    (function() {
                        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                    })();
                }
            });
        }
    };

})(jQuery);

