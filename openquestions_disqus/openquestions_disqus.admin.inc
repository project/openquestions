<?php

/**
 * @file
 * Contains admin functions for OpenQuestions.
 */

/**
 * Settings form for this module.
 *
 * @see system_settings_form()
 */
function openquestions_disqus_settings() {
  $form['openquestions_disqus_add_to_user'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Disqus comments on user profiles?'),
    '#default_value' => variable_get('openquestions_disqus_add_to_user', ''),
    '#description' => t('Whether to show Disqus comments on user profiles or not.'),
  );

  $form['openquestions_disqus_add_to_node'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Disqus comments on nodes?'),
    '#default_value' => variable_get('openquestions_disqus_add_to_node', ''),
    '#description' => t('Whether to show Disqus comments on nodes or not. Set the node type below.'),
  );

  $form['openquestions_disqus_add_to_entity'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Disqus comments on entities?'),
    '#default_value' => variable_get('openquestions_disqus_add_to_entity', ''),
    '#description' => t('Whether to show Disqus comments on entities or not. Set the entity type below.'),
  );

  $form['openquestions_disqus_shortname'] = array(
    '#type' => 'textfield',
    '#title' => t('Disqus shortname'),
    '#default_value' => variable_get('openquestions_disqus_shortname', ''),
    '#description' => t('Get this from the Disqus website after you signed up.'),
  );

  $form['openquestions_disqus_node_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Node type'),
    '#default_value' => variable_get('openquestions_disqus_node_type', ''),
    '#description' => t('The type of nodes to show Disqus comments on, usually "oq_question".'),
  );

  $form['openquestions_disqus_entity_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Entity type'),
    '#default_value' => variable_get('openquestions_disqus_entity_type', ''),
    '#description' => t('The type of the entity to show Disqus comments on, usually "while".'),
  );

  return system_settings_form($form);
}
