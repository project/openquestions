<?php
/**
 * @file
 * Views template.
 */
?>
<div class="expertvote_vote">
<?php print $fields['name']->content; ?> voted <?php print $fields['field_oq_vote']->content; ?>
 (<a href="<?php print $fields['url']->content; ?>"><?php print $fields['changed']->content; ?></a>)
<input type="hidden" name="expertvote_vote_entityid" value="<?php print $fields['id']->content; ?>" />
<input type="hidden" name="expertvote_vote_uid" value="<?php print $fields['uid']->content; ?>" />
</div>
