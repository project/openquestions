<?php
/**
 * @file
 * Views template.
 */
?>
<div class="expertvote_report">
<?php print $fields['name']->content; ?> reported <?php print $fields['title']->content; ?> as
 <?php print $fields['field_oq_reporttype']->content; ?>
 (<a href="<?php print $fields['url']->content; ?>"><?php print $fields['changed']->content; ?></a>)
<input type="hidden" name="expertvote_report_entityid" value="<?php print $fields['id']->content; ?>" />
<input type="hidden" name="expertvote_report_uid" value="<?php print $fields['uid']->content; ?>" />
</div>
