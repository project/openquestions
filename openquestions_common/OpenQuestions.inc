<?php

/**
 * @file
 * Contains a helper class for OpenQuestions.
 */

/**
 * Helper class for OpenQuestions.
 */
class OpenQuestionsCommon {
  const ENTITY_NAME = 'while';
  const BUNDLE_NAME = 'oq_expert_vote';
  const REVISION_TABLE = 'while_revision';
  const FIELD_COLUMN_FORQUESTION = 'target_id';
  const FIELD_COLUMN_FROMUSER = 'uid';
  const OPENQUESTIONS_QUESTION_NODE_TYPE = 'oq_question';

  private $uid;
  private $nid;

  /**
   * Constructor.
   *
   * @param int $u
   *   A uid.
   * @param int $n
   *   A nid.
   */
  public function __construct($u, $n) {
    if ($u < 1 || $n < 1) {
      throw new Exception('Bad arguments');
    }

    $this->uid = $u;
    $this->nid = $n;

    $temp_user = user_load($this->uid);
    $temp_node = node_load($this->nid);
    if (empty($temp_user->name) || empty($temp_node->title)) {
      throw new Exception('Bad arguments');
    }

    // Using ! to avoid double-encoding. entityLabel is saved with entities,
    // it isn't output by this class.
    $this->entityLabel = t('!name opinion of !title', array(
    '!name' => $temp_user->name,
    '!title' => $temp_node->title,
    ));
  }

  /**
   * Returns the latest revision, or NULL.
   */
  public function getLatestRevision() {
    $results = $this->getAllRevisionsRaw();
    if (!$results || empty($results[self::getEntityName()])) {
      return NULL;
    }

    $results = $results[self::getEntityName()];

    if (count($results) > 1) {
      usort($results, array('OpenQuestionsCommon', 'compareRevisionIds'));
    }

    $obj = array_shift($results);

    return !empty($obj->id) ? entity_load_single(self::getEntityName(), $obj->id) : NULL;
  }

  /**
   * Returns the latest vote as an integer, or NULL.
   */
  public function getVote() {
    $entity = $this->getLatestRevision();
    if (!$entity) {
      return NULL;
    }

    $wrapper = entity_metadata_wrapper(self::getEntityName(), $entity);
    $vote_value = $wrapper->field_oq_vote->value();

    return $vote_value;
  }

  /**
   * Creates a new vote with the given value.
   *
   * If the user has already taken action (voted on or reported) this node,
   * then a new revision is created. Otherwise, an entirely new entity is
   * created.
   */
  public function createVote($vote_value) {
    $entity = $this->getLatestRevision();

    if ($entity) {
      $wrapper = entity_metadata_wrapper(self::getEntityName(), $entity);
      $wrapper->field_oq_vote->set($vote_value);
      $wrapper->field_oq_reporttype->set(NULL);
      $wrapper->field_oq_reporttypedata->set('');
      $entity->options['create_revision'] = TRUE;
      $entity->save();
    }
    else {
      $properties = array(
        'type' => self::getBundleName(),
        'label' => $this->entityLabel,
        'status' => 1,
        'uid' => $this->uid,
      );
      $entity = entity_create(self::getEntityName(), $properties);

      $wrapper = entity_metadata_wrapper(self::getEntityName(), $entity);
      $wrapper->field_oq_forquestion->set($this->nid);
      $wrapper->field_oq_fromuser->set($this->uid);
      $wrapper->field_oq_vote->set($vote_value);
      $wrapper->field_oq_reporttype->set(NULL);
      $wrapper->field_oq_reporttypedata->set('');
      $wrapper->save();
    }
  }

  /**
   * Creates a new report with the given tid and text description.
   *
   * If the user has already taken action (voted on or reported) this node,
   * then a new revision is created. Otherwise, an entirely new entity is
   * created.
   */
  public function createReport($report_type_tid, $report_data) {
    $entity = $this->getLatestRevision();

    if ($entity) {
      $wrapper = entity_metadata_wrapper(self::getEntityName(), $entity);
      $wrapper->field_oq_reporttype->set($report_type_tid);
      $wrapper->field_oq_reporttypedata->set($report_data);
      $wrapper->field_oq_vote->set(NULL);
      $entity->options['create_revision'] = TRUE;
      $entity->save();
    }
    else {
      $properties = array(
        'type' => self::getBundleName(),
        'label' => $this->entityLabel,
        'status' => 1,
        'uid' => $this->uid,
      );
      $entity = entity_create(self::getEntityName(), $properties);

      $wrapper = entity_metadata_wrapper(self::getEntityName(), $entity);
      $wrapper->field_oq_forquestion->set($this->nid);
      $wrapper->field_oq_fromuser->set($this->uid);
      $wrapper->field_oq_reporttype->set($report_type_tid);
      $wrapper->field_oq_reporttypedata->set($report_data);
      $wrapper->field_oq_vote->set(NULL);
      $wrapper->save();
    }
  }

  /**
   * Returns an array of all revisions, or NULL.
   */
  public function getAllRevisions() {
    $results = $this->getAllRevisionsRaw();
    if (!$results || empty($results[self::getEntityName()])) {
      return NULL;
    }

    $results = $results[self::getEntityName()];
    $ret = array();

    if (count($results) > 1) {
      usort($results, array('OpenQuestionsCommon', 'compareRevisionIds'));
    }

    foreach ($results as $obj) {
      $ret[$obj->revision_id] = entity_revision_load(self::getEntityName(), $obj->revision_id);
    }

    return $ret;
  }

  /**
   * Returns a structured array containing all revisions, or NULL.
   */
  public function getVoteHistory() {
    $entities = $this->getAllRevisions();
    if (!$entities || empty($entities)) {
      return NULL;
    }

    $ret = array();
    foreach ($entities as $revision_id => $entity) {
      $wrapper = entity_metadata_wrapper(self::getEntityName(), $entity);
      $ret[$revision_id] = array(
        'created' => $entity->created,
        'changed' => $entity->changed,
        'vote' => $wrapper->field_oq_vote->value(),
      );
    }

    return $ret;
  }

  /**
   * Returns a database results object with all revisions.
   */
  public function getAllRevisionsRaw() {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', self::getEntityName())
      ->fieldCondition('field_oq_forquestion', self::FIELD_COLUMN_FORQUESTION, $this->nid)
      ->fieldCondition('field_oq_fromuser', self::FIELD_COLUMN_FROMUSER, $this->uid)
      ->age(FIELD_LOAD_REVISION);

    $results = $query->execute();

    return $results;
  }

  /**
   * Returns the uid.
   */
  public function getUid() {
    return $this->uid;
  }

  /**
   * Returns the nid.
   */
  public function getNid() {
    return $this->nid;
  }

  /**
   * Sets the uid.
   */
  public function setUid($u) {
    $this->uid = $u;
  }

  /**
   * Sets the nid.
   */
  public function setNid($n) {
    $this->nid = $n;
  }

  /**
   * Sorting helper method.
   */
  public static function compareRevisionIds($a, $b) {
    if ($a->revision_id == $b->revision_id) {
      return 0;
    }

    return $a->revision_id < $b->revision_id;
  }

  /**
   * Returns e.g. "oq_question".
   */
  public static function getQuestionNodeType() {
    return self::OPENQUESTIONS_QUESTION_NODE_TYPE;
  }

  /**
   * Returns e.g. "while".
   */
  public static function getEntityName() {
    return self::ENTITY_NAME;
  }

  /**
   * Returns e.g. "oq_expert_vote".
   */
  public static function getBundleName() {
    return self::BUNDLE_NAME;
  }

  /**
   * Returns array for 'select' form items with the Report Types terms.
   */
  public static function getReportTermsOptions() {
    $vid = variable_get('openquestions_vocab_report_types', 0);
    if (!$vid) {
      $vocab = taxonomy_vocabulary_machine_name_load('oq_report_type');
      if (empty($vocab->vid)) {
        return NULL;
      }
      $vid = $vocab->vid;
      variable_set('openquestions_vocab_report_types', $vid);
    }

    $report_terms = taxonomy_get_tree($vid);
    if (!$report_terms) {
      return NULL;
    }

    $ret = array('0' => t('None'));
    foreach ($report_terms as $report_term) {
      $ret[$report_term->tid] = $report_term->name;
    }

    return $ret;
  }

  /**
   * Returns array for 'select' form items with possible votes.
   */
  public static function getVoteChoiceOptions() {
    $min_vote = self::getMinVoteValue();
    $max_vote = self::getMaxVoteValue();

    $ret = array('0' => t('None'));

    for ($i = $min_vote; $i <= $max_vote; $i++) {
      $ret[$i] = $i;
    }

    return $ret;
  }

  /**
   * Returns e.g. 1.
   */
  public static function getMinVoteValue() {
    return variable_get('openquestions_min_vote', 1);
  }

  /**
   * Returns e.g. 10.
   */
  public static function getMaxVoteValue() {
    return variable_get('openquestions_max_vote', 10);
  }

}
