<?php
/**
 * @file
 * openquestions_features.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function openquestions_features_user_default_roles() {
  $roles = array();

  // Exported role: oq_expert.
  $roles['oq_expert'] = array(
    'name' => 'oq_expert',
    'weight' => 3,
    'machine_name' => 'oq_expert',
  );

  return $roles;
}
