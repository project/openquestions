<?php
/**
 * @file
 * openquestions_features.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function openquestions_features_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-oq_question-body'
  $field_instances['node-oq_question-body'] = array(
    'bundle' => 'oq_question',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Enter your question here.

Guidelines:

<ul>
<li>Make sure this is a specific question, not a vague question or a statement.</li>
<li>Try to be as brief as possible, unless it makes the question less tough.</li>
<li>Don\'t ask questions about personal issues, just about policy.</li>
</ul>',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Question',
    'required' => 0,
    'settings' => array(
      'display_summary' => 0,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-oq_question-field_oq_addressee'
  $field_instances['node-oq_question-field_oq_addressee'] = array(
    'bundle' => 'oq_question',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Who should be asked this question?

You can enter one or more names, all in lowercase letters. If you enter multiple names, separate them by commas.

After entering a name, please wait for the suggestion to appear and choose it if it does. Sample input: hillary clinton, nancy pelosi.

If the question isn\'t for a specific politician, use a form like one of the following:
any politician
any democrat
any republican
any senator
(and so on...)

Entering too many names or misspelling names may cause your question to be deleted.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_oq_addressee',
    'label' => 'Addressee',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'taxonomy',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'taxonomy_autocomplete',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-oq_question-field_oq_topic'
  $field_instances['node-oq_question-field_oq_topic'] = array(
    'bundle' => 'oq_question',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The general topic of this question. If it doesn\'t fit any of the other topics, choose Miscellaneous.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_oq_topic',
    'label' => 'Topic',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-oq_question-field_supporting_information'
  $field_instances['node-oq_question-field_supporting_information'] = array(
    'bundle' => 'oq_question',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Enter any supporting information here. For instance, if asking about a statement a politician made, enter a link to the video or news report where the politician made the statement. Quote the relevant part of statement, including the context.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_supporting_information',
    'label' => 'Supporting information',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'user-user-field_apply_for_oq_expert_status'
  $field_instances['user-user-field_apply_for_oq_expert_status'] = array(
    'bundle' => 'user',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'Check this if you want to apply for expert status. If approved, you\'ll be able to vote on questions.

Make sure and choose one or more "Expertise" items, provide your website and/or social media addresses, and provide some information on your background.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_apply_for_oq_expert_status',
    'label' => 'Apply for expert status',
    'required' => 0,
    'settings' => array(
      'user_register_form' => 1,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'user-user-field_oq_expertise'
  $field_instances['user-user-field_oq_expertise'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'If applying for expert status, choose the field(s) you\'re an expert in.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_oq_expertise',
    'label' => 'Expertise',
    'required' => 0,
    'settings' => array(
      'user_register_form' => 1,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'user-user-field_oq_qualifications'
  $field_instances['user-user-field_oq_qualifications'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'If applying for expert status, enter some background information on your qualifications.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_oq_qualifications',
    'label' => 'Qualifications',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => 1,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'user-user-field_oq_social_links'
  $field_instances['user-user-field_oq_social_links'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'socialfield',
        'settings' => array(
          'link_text' => 1,
        ),
        'type' => 'socialfield_formatter',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_oq_social_links',
    'label' => 'Social links',
    'required' => 0,
    'settings' => array(
      'services' => array(
        'facebook' => 'facebook',
        'googleplus' => 'googleplus',
        'linkedin' => 'linkedin',
        'twitter' => 'twitter',
        'website' => 'website',
        'youtube' => 'youtube',
      ),
      'used_services' => array(
        'delicious' => 'delicious',
        'digg' => 'digg',
        'facebook' => 'facebook',
        'flickr' => 'flickr',
        'googleplus' => 'googleplus',
        'linkedin' => 'linkedin',
        'myspace' => 'myspace',
        'pinterest' => 'pinterest',
        'reddit' => 'reddit',
        'slideshare' => 'slideshare',
        'stumbleupon' => 'stumbleupon',
        'twitter' => 'twitter',
        'vimeo' => 'vimeo',
        'website' => 'website',
        'yahoo' => 'yahoo',
        'youtube' => 'youtube',
      ),
      'user_register_form' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'socialfield',
      'settings' => array(),
      'type' => 'socialfield_widget',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'while-oq_expert_vote-field_oq_forquestion'
  $field_instances['while-oq_expert_vote-field_oq_forquestion'] = array(
    'bundle' => 'oq_expert_vote',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => 1,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
      'full' => array(
        'label' => 'inline',
        'module' => 'entityreference',
        'settings' => array(
          'link' => 1,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'while',
    'field_name' => 'field_oq_forquestion',
    'label' => 'For question',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'while-oq_expert_vote-field_oq_fromuser'
  $field_instances['while-oq_expert_vote-field_oq_fromuser'] = array(
    'bundle' => 'oq_expert_vote',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'user_reference',
        'settings' => array(),
        'type' => 'user_reference_default',
        'weight' => 2,
      ),
      'full' => array(
        'label' => 'inline',
        'module' => 'user_reference',
        'settings' => array(),
        'type' => 'user_reference_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'while',
    'field_name' => 'field_oq_fromuser',
    'label' => 'From user',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'while-oq_expert_vote-field_oq_reporttype'
  $field_instances['while-oq_expert_vote-field_oq_reporttype'] = array(
    'bundle' => 'oq_expert_vote',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 3,
      ),
      'full' => array(
        'label' => 'inline',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_plain',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'while',
    'field_name' => 'field_oq_reporttype',
    'label' => 'Report type',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'while-oq_expert_vote-field_oq_reporttypedata'
  $field_instances['while-oq_expert_vote-field_oq_reporttypedata'] = array(
    'bundle' => 'oq_expert_vote',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
      'full' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_plain',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'while',
    'field_name' => 'field_oq_reporttypedata',
    'label' => 'Report type data',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'while-oq_expert_vote-field_oq_vote'
  $field_instances['while-oq_expert_vote-field_oq_vote'] = array(
    'bundle' => 'oq_expert_vote',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'while',
    'field_name' => 'field_oq_vote',
    'label' => 'Vote',
    'required' => FALSE,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Addressee');
  t('Apply for expert status');
  t('Check this if you want to apply for expert status. If approved, you\'ll be able to vote on questions.

Make sure and choose one or more "Expertise" items, provide your website and/or social media addresses, and provide some information on your background.');
  t('Enter any supporting information here. For instance, if asking about a statement a politician made, enter a link to the video or news report where the politician made the statement. Quote the relevant part of statement, including the context.');
  t('Enter your question here.

Guidelines:

<ul>
<li>Make sure this is a specific question, not a vague question or a statement.</li>
<li>Try to be as brief as possible, unless it makes the question less tough.</li>
<li>Don\'t ask questions about personal issues, just about policy.</li>
</ul>');
  t('Expertise');
  t('For question');
  t('From user');
  t('If applying for expert status, choose the field(s) you\'re an expert in.');
  t('If applying for expert status, enter some background information on your qualifications.');
  t('Qualifications');
  t('Question');
  t('Report type');
  t('Report type data');
  t('Social links');
  t('Supporting information');
  t('The general topic of this question. If it doesn\'t fit any of the other topics, choose Miscellaneous.');
  t('Topic');
  t('Vote');
  t('Who should be asked this question?

You can enter one or more names, all in lowercase letters. If you enter multiple names, separate them by commas.

After entering a name, please wait for the suggestion to appear and choose it if it does. Sample input: hillary clinton, nancy pelosi.

If the question isn\'t for a specific politician, use a form like one of the following:
any politician
any democrat
any republican
any senator
(and so on...)

Entering too many names or misspelling names may cause your question to be deleted.');

  return $field_instances;
}
