<?php
/**
 * @file
 * openquestions_features.features.roles_permissions.inc
 */

/**
 * Implements hook_default_roles_permissions().
 */
function openquestions_features_default_roles_permissions() {
  $roles = array();

  // Exported role: oq_expert
  $roles['oq_expert'] = array(
    'name' => 'oq_expert',
    'weight' => 3,
    'permissions' => array(
      'create oq_question content' => TRUE,
      'edit own oq_question content' => TRUE,
      'openquestions vote on nodes' => TRUE,
    ),
  );

  return $roles;
}
