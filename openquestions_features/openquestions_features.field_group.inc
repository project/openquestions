<?php
/**
 * @file
 * openquestions_features.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function openquestions_features_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_oq_expert_status|user|user|form';
  $field_group->group_name = 'group_oq_expert_status';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Expert status',
    'weight' => '7',
    'children' => array(
      0 => 'field_apply_for_oq_expert_status',
      1 => 'field_oq_expertise',
      2 => 'field_oq_qualifications',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Expert status',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-oq_expert-status field-group-fieldset',
        'description' => 'Fill this out if you want to apply for expert status (so you can vote on questions).',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_oq_expert_status|user|user|form'] = $field_group;

  return $export;
}
