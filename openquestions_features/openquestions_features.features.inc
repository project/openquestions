<?php
/**
 * @file
 * openquestions_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function openquestions_features_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function openquestions_features_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function openquestions_features_node_info() {
  $items = array(
    'oq_question' => array(
      'name' => t('Question'),
      'base' => 'node_content',
      'description' => t('A user-submitted tough question for one or more politicians.'),
      'has_title' => '1',
      'title_label' => t('One line summary'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_default_while_type().
 */
function openquestions_features_default_while_type() {
  $items = array();
  $items['oq_expert_vote'] = entity_import('while_type', '{
    "label" : "Expert vote",
    "weight" : "0",
    "name" : "oq_expert_vote",
    "data" : { "supports_revisions" : 1 },
    "rdf_mapping" : []
  }');
  return $items;
}
