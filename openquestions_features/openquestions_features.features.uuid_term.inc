<?php
/**
 * @file
 * openquestions_features.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function openquestions_features_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Euthanasia',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '0a356955-11d8-43d7-9258-9994a218658d',
    'vocabulary_machine_name' => 'oq_topic',
  );
  $terms[] = array(
    'name' => 'Miscellaneous',
    'description' => 'In case this doesn\'t fit into one of the other topics.',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '0acf6b1f-ab84-43c3-a29a-aec1ef674529',
    'vocabulary_machine_name' => 'oq_topic',
  );
  $terms[] = array(
    'name' => 'Environment',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '0f65acd4-acfd-4bac-8be1-34595612a47e',
    'vocabulary_machine_name' => 'oq_topic',
  );
  $terms[] = array(
    'name' => 'First amendment',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '39bb1e7d-1ebf-4cd3-ba64-d8cba44f802f',
    'vocabulary_machine_name' => 'oq_topic',
  );
  $terms[] = array(
    'name' => 'Second amendment',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '3a1648a9-5d25-45e0-a7bc-becbbb607eb6',
    'vocabulary_machine_name' => 'oq_topic',
  );
  $terms[] = array(
    'name' => 'Economy',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '5ce290a7-af9d-48a5-87b1-a5ffabd13c44',
    'vocabulary_machine_name' => 'oq_topic',
  );
  $terms[] = array(
    'name' => 'Not applicable',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '66456550-5cf6-4f5d-a1c9-1b8bfc72591b',
    'vocabulary_machine_name' => 'oq_report_type',
  );
  $terms[] = array(
    'name' => 'Energy',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '66bc8db4-1c55-4898-8acb-e46cb96289d6',
    'vocabulary_machine_name' => 'oq_topic',
  );
  $terms[] = array(
    'name' => 'Abortion',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '7423bb43-02b0-4076-8456-d324452def51',
    'vocabulary_machine_name' => 'oq_topic',
  );
  $terms[] = array(
    'name' => 'Trade',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '8826a4a9-83d4-4ec7-b79f-ab6997c13b66',
    'vocabulary_machine_name' => 'oq_topic',
  );
  $terms[] = array(
    'name' => 'Not a question',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '91b5e118-9869-403b-b4da-32d6bc1aa450',
    'vocabulary_machine_name' => 'oq_report_type',
  );
  $terms[] = array(
    'name' => 'Education',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'a67cf6bc-2fee-464e-9443-8de1bbc3bc89',
    'vocabulary_machine_name' => 'oq_topic',
  );
  $terms[] = array(
    'name' => 'Spam',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'b4abc82e-c73c-4a60-b61f-ebf254661780',
    'vocabulary_machine_name' => 'oq_report_type',
  );
  $terms[] = array(
    'name' => 'Multiculturalism',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'b968ada4-becb-4351-8221-f890c30289bb',
    'vocabulary_machine_name' => 'oq_topic',
  );
  $terms[] = array(
    'name' => 'Immigration',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'bf7a57a4-79d2-4d56-b5ab-ec941f751e77',
    'vocabulary_machine_name' => 'oq_topic',
  );
  $terms[] = array(
    'name' => 'Duplicate',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'ca662101-6ec8-4def-aff0-6c525bf74cdd',
    'vocabulary_machine_name' => 'oq_report_type',
  );
  $terms[] = array(
    'name' => 'Healthcare',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'e4553765-e83a-4398-b0c3-49234331b6ec',
    'vocabulary_machine_name' => 'oq_topic',
  );
  $terms[] = array(
    'name' => 'Foreign policy',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'f3287787-7101-413e-832c-c0bb34c4872e',
    'vocabulary_machine_name' => 'oq_topic',
  );
  return $terms;
}
